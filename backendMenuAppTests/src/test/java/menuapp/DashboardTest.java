package menuapp;

import com.MenuAppPO;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class DashboardTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
    }

    @Test
    public void verifyDashboardLinksAsLocation() throws IOException {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectDashboardFromNavBar();

        // Verify Username appears on Dashboard
        String welmsg = driver.findElement(By.className("welcomeh1")).getText();
        assertThat(welmsg, containsString(menuAppPO.USERNAME_MENUAPP));

        menuAppPO.selectSetupLocationsFromDashboard();

        // Verify Locations page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Locations"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectImportAndSetUpYourMenu();

        // Verify 'Import and set up your menu'
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Import From POS"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectUsersAndPermissionsFromDashboard();

        // Verify 'Users' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Users"));

        menuAppPO.selectDashboardFromNavBar();

        /*
        menuAppPO.selectRulesFromDashboard();

        // Verify 'Rules' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Rules"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectSurveyFromDashboard();

        // Verify 'Surveys' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Surveys"));

        menuAppPO.selectDashboardFromNavBar();
        */

        menuAppPO.selectEditYourMenusFromDashboard();

        // Verify 'Edit your Menus' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Menus"));

        menuAppPO.selectDashboardFromNavBar();

        /*
        menuAppPO.selectImportManagerFromDashboard();

        // Verify 'Import Manager' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Import From POS"));

        menuAppPO.selectDashboardFromNavBar();
        */

        // Verify Location appears
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectNavLogo();

        // Verify Admin Location appears
        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

    }

    @Test
    public void verifyDashboardLinksAsAdmin() throws IOException {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        // Verify Admin Location
        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        // Verify Username appears on Dashboard
        String welmsg = driver.findElement(By.className("welcomeh1")).getText();
        assertThat(welmsg, containsString(menuAppPO.USERNAME_MENUAPP));

        menuAppPO.selectSetupLocationsFromDashboard();

        // Verify Locations page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Locations"));

        menuAppPO.selectNavLogo();

        menuAppPO.selectThemesFromDashboard();

        // Verify 'Customize MenuPad Branding' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Themes"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectImportAndSetUpYourMenu();

        // Verify 'Import and set up your menu' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Import From POS"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectUsersAndPermissionsFromDashboard();

        // Verify 'Import and set up your menu' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Users"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectRulesFromDashboard();

        // Verify 'Rules' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Rules"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectSurveyFromDashboard();

        // Verify 'Surveys' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Surveys"));

        menuAppPO.selectDashboardFromNavBar();

        menuAppPO.selectEditYourMenusFromDashboard();

        // Verify 'Edit your Menus' page appears
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Menus"));

        menuAppPO.selectDashboardFromNavBar();
    }

    @Test
    public void tearDown() {
        driver.quit();
    }
}
