package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class MenuItemsEditTest {

    WebElement multSelectOption;
    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Test
    public void editMenuItemLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Check if Item already exists
        List<WebElement> testItemBefore = driver.findElements(By.xpath("//*[contains(text(), 'ItemEdit')]"));
        System.out.println("Item: " + testItemBefore.size());

        menuAppPO.selectAddItem();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Section: " + menuAppPO.section));

        // Enter Item Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.itemToEdit);

        // Enter Short Description
        driver.findElement(By.id("id_short_desc")).sendKeys("Short Descrip Test");

        // Enter Long Description
        driver.findElement(By.id("id_long_desc")).sendKeys("Long Descript Test");

        // Enter Price Level 1
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys("2.00");

        // Enter POS code
        driver.findElement(By.id("id_pos_code")).sendKeys("1234");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        menuAppPO.tapSectionInBreadcrumb();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfter = driver.findElements(By.xpath("//*[contains(text(), 'ItemEdit')]"));
        System.out.println("Item: " + testItemAfter.size());

        if (testItemAfter.size() > testItemBefore.size()) {
            // do nothing - item added successfully
        } else throw new Exception("Item is not added!");

        // Select Item
        testItemAfter.get(testItemAfter.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.itemToEdit));

        // Edit Short Description
        driver.findElement(By.id("id_short_desc")).sendKeys("Edits");

        // Edit Long Description
        driver.findElement(By.id("id_long_desc")).sendKeys("Edits");

        // Edit Price Level 1
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys("4.25");

        // Edit POS code
        driver.findElement(By.id("id_pos_code")).sendKeys("5");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectItemCategories();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_categories")));

        // Verify Item Category (2) is not checked
        Boolean isCheckedItemCatOne = driver.findElement(By.id("id_categories_1")).isSelected();
        assertFalse(isCheckedItemCatOne);

        // Check Item Category
        driver.findElement(By.id("id_categories_1")).click();

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectFoodOptions();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_option_group")));

        multSelectOption = driver.findElement(By.cssSelector("select[id='id_option_group']"));
        Select clickM = new Select(multSelectOption);

        // Select Option Value
        clickM.selectByVisibleText(menuAppPO.optionValue);

        // Select 'Apply Option Group'
        driver.findElement(By.id("option_group_apply")).click();

        Thread.sleep(1000);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectItemMatching();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("existing-matches")));

        // Get Current Matches
        String iMatchBefore = driver.findElement(By.xpath(".//*[@id='existing-matches']/li")).getText();
        System.out.println("Current Matches: " + iMatchBefore);

        // Expand Accordion
        // driver.findElement(By.id("ui-accordion-accordion-header-0")).click();

        Thread.sleep(1000);

        // Verify Item is not selected
        Boolean isCheckedItemMatchingPre = driver.findElement(By.className("matches-item")).isSelected();
        assertFalse(isCheckedItemMatchingPre);

        // Select Item Match
        driver.findElement(By.className("matches-item")).click();

        // Verify Item is selected
        Boolean isCheckedItemMatching = driver.findElement(By.className("matches-item")).isSelected();
        assertTrue(isCheckedItemMatching);

        // Tap 'Done'
        // driver.findElement(By.linkText("Done")).click();
        // driver.findElement(By.xpath("//*[contains(text(), 'Done')]")).click();

        // Verify Item Matches shown
        // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='existing-matches']/li[2]")));

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectItemRules();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_rules_0")));

        // Verify Item Rule is not checked
        Boolean isCheckedItemRule = driver.findElement(By.id("id_rules_0")).isSelected();
        assertFalse(isCheckedItemRule);

        // Select Rule
        driver.findElement(By.id("id_rules_0")).click();

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectMenus();

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfterEdits = driver.findElements(By.xpath("//*[contains(text(), 'ItemEdit')]"));
        System.out.println("Item: " + testItemAfterEdits.size());

        if (testItemAfterEdits.size() == testItemAfter.size()) {
            // do nothing - item added successfully
        } else throw new Exception("Item count is incorrect!");

        /////////////////////////////// Verify Edits /////////////////////////////////////

        // Select Item
        testItemAfterEdits.get(testItemAfterEdits.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.itemToEdit));

        // Verify 'Short Description' saved successfully
        WebElement textShort = driver.findElement(By.id("id_short_desc"));
        String textShortDescrip = textShort.getAttribute("value");
        assertEquals("Short Descrip TestEdits", textShortDescrip);

        // Verify 'Long Description' saved successfully
        WebElement textLong = driver.findElement(By.id("id_long_desc"));
        String textLongDescrip = textLong.getAttribute("value");
        assertEquals("Long Descript TestEdits", textLongDescrip);

        // Verify 'Price'
        assertEquals("4.25", driver.findElement(By.id("id_price")).getAttribute("value"));

        // Verify 'POS code'
        assertEquals("12345", driver.findElement(By.id("id_pos_code")).getAttribute("value"));

        menuAppPO.selectItemCategories();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_categories")));

        // Verify Item Category (2) is checked
        Boolean isCheckedItemCatOnePostEdit = driver.findElement(By.id("id_categories_1")).isSelected();
        assertTrue(isCheckedItemCatOnePostEdit);

        menuAppPO.selectFoodOptions();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_option_group")));

        wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("id_form-0-name"), "Add a soup or salad"));

        menuAppPO.selectItemMatching();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("existing-matches")));

        // Get Current Matches
        String iMatchAfter = driver.findElement(By.xpath(".//*[@id='existing-matches']/li")).getText();
        System.out.println("Current Matches(After: " + iMatchAfter);

        // Verify Current Match updates correctly
        assertFalse(iMatchBefore.equalsIgnoreCase(iMatchAfter));

        // Expand Accordion
        // driver.findElement(By.id("ui-accordion-accordion-header-0")).click();

        Thread.sleep(1000);

        // Verify Match is selected
        Boolean isCheckedItemMatchingPost = driver.findElement(By.className("matches-item")).isSelected();
        assertTrue(isCheckedItemMatchingPost);

        menuAppPO.selectItemRules();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_rules_0")));

        // Verify Item Rule is not checked
        Boolean isCheckedItemRulePostEdit = driver.findElement(By.id("id_rules_0")).isSelected();
        assertTrue(isCheckedItemRulePostEdit);

        System.out.println("Deleting Item....");

        driver.findElement(By.cssSelector("div[name='delete']")).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("delete"))).click();

        menuAppPO.acceptAlert();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();
    }

    @Test
    public void editMenuItemAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectMenus();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.adminLocations + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Check if Item already exists
        List<WebElement> testItemBefore = driver.findElements(By.xpath("//*[contains(text(), 'ItemEdit')]"));
        System.out.println("Item: " + testItemBefore.size());

        menuAppPO.selectAddItem();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Section: " + menuAppPO.section));

        // Enter Item Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.itemToEdit);

        // Enter Short Description
        driver.findElement(By.id("id_short_desc")).sendKeys("Short Descrip Test");

        // Enter Long Description
        driver.findElement(By.id("id_long_desc")).sendKeys("Long Descript Test");

        // Enter Price Level 1
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys("2.00");

        // Enter POS code
        driver.findElement(By.id("id_pos_code")).sendKeys("1234");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        // Tap Section in breadcrumb
        driver.findElement(By.linkText(menuAppPO.section)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfter = driver.findElements(By.xpath("//*[contains(text(), 'ItemEdit')]"));
        System.out.println("Item: " + testItemAfter.size());

        if (testItemAfter.size() > testItemBefore.size()) {
            // do nothing - item added successfully
        } else throw new Exception("Item is not added!");

        // Select Item
        testItemAfter.get(testItemAfter.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.itemToEdit));

        // Edit Short Description
        driver.findElement(By.id("id_short_desc")).sendKeys("Edits");

        // Edit Long Description
        driver.findElement(By.id("id_long_desc")).sendKeys("Edits");

        // Edit Price Level 1
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys("4.25");

        // Edit POS code
        driver.findElement(By.id("id_pos_code")).sendKeys("5");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectItemCategories();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_categories")));

        // Verify Item Category (2) is not checked
        Boolean isCheckedItemCatOne = driver.findElement(By.id("id_categories_1")).isSelected();
        assertFalse(isCheckedItemCatOne);

        // Check Item Category
        driver.findElement(By.id("id_categories_1")).click();

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectFoodOptions();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_option_group")));

        multSelectOption = driver.findElement(By.cssSelector("select[id='id_option_group']"));
        Select clickM = new Select(multSelectOption);

        // Select Option Value
        clickM.selectByVisibleText(menuAppPO.optionValue);

        // Select 'Apply Option Group'
        driver.findElement(By.id("option_group_apply")).click();

        Thread.sleep(1000);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectItemMatching();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("existing-matches")));

        // Get Current Matches
        String iMatchBefore = driver.findElement(By.xpath(".//*[@id='existing-matches']/li")).getText();
        System.out.println("Current Matches: " + iMatchBefore);

        // Expand Accordion
        // driver.findElement(By.id("ui-accordion-accordion-header-0")).click();

        Thread.sleep(1000);

        // Verify Item is not selected
        Boolean isCheckedItemMatchingPre = driver.findElement(By.className("matches-item")).isSelected();
        assertFalse(isCheckedItemMatchingPre);

        // Select Item Match
        driver.findElement(By.className("matches-item")).click();

        // Verify Item is selected
        Boolean isCheckedItemMatching = driver.findElement(By.className("matches-item")).isSelected();
        assertTrue(isCheckedItemMatching);

        // Tap 'Done'
        // driver.findElement(By.linkText("Done")).click();
        // driver.findElement(By.xpath("//*[contains(text(), 'Done')]")).click();

        // Verify Item Matches shown
        // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='existing-matches']/li[2]")));

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectItemRules();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_rules_0")));

        // Verify Item Rule is not checked
        Boolean isCheckedItemRule = driver.findElement(By.id("id_rules_0")).isSelected();
        assertFalse(isCheckedItemRule);

        // Select Rule
        driver.findElement(By.id("id_rules_0")).click();

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectMenus();

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfterEdits = driver.findElements(By.xpath("//*[contains(text(), 'ItemEdit')]"));
        System.out.println("Item: " + testItemAfterEdits.size());

        if (testItemAfterEdits.size() == testItemAfter.size()) {
            // do nothing - item added successfully
        } else throw new Exception("Item count is incorrect!");

        /////////////////////////////// Verify Edits /////////////////////////////////////

        // Select Item
        testItemAfterEdits.get(testItemAfterEdits.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.itemToEdit));

        // Verify 'Short Description' saved successfully
        WebElement textShort = driver.findElement(By.id("id_short_desc"));
        String textShortDescrip = textShort.getAttribute("value");
        assertEquals("Short Descrip TestEdits", textShortDescrip);

        // Verify 'Long Description' saved successfully
        WebElement textLong = driver.findElement(By.id("id_long_desc"));
        String textLongDescrip = textLong.getAttribute("value");
        assertEquals("Long Descript TestEdits", textLongDescrip);

        // Verify 'Price'
        assertEquals("4.25", driver.findElement(By.id("id_price")).getAttribute("value"));

        // Verify 'POS code'
        assertEquals("12345", driver.findElement(By.id("id_pos_code")).getAttribute("value"));

        // Select 'Item Categories'
        driver.findElement(By.cssSelector("div[data-form='item-categories']")).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_categories")));

        // Verify Item Category (2) is checked
        Boolean isCheckedItemCatOnePostEdit = driver.findElement(By.id("id_categories_1")).isSelected();
        assertTrue(isCheckedItemCatOnePostEdit);

        menuAppPO.selectFoodOptions();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_option_group")));

        wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("id_form-0-name"), "Add a soup or salad"));

        menuAppPO.selectItemMatching();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("existing-matches")));

        // Get Current Matches
        String iMatchAfter = driver.findElement(By.xpath(".//*[@id='existing-matches']/li")).getText();
        System.out.println("Current Matches(After: " + iMatchAfter);

        // Verify Current Match updates correctly
        assertFalse(iMatchBefore.equalsIgnoreCase(iMatchAfter));

        // Expand Accordion
        // driver.findElement(By.id("ui-accordion-accordion-header-0")).click();

        Thread.sleep(1000);

        // Verify Match is selected
        Boolean isCheckedItemMatchingPost = driver.findElement(By.className("matches-item")).isSelected();
        assertTrue(isCheckedItemMatchingPost);

        // Verify Item Matches shown
        // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='existing-matches']/li[2]")));

        menuAppPO.selectItemRules();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_rules_0")));

        // Verify Item Rule is not checked
        Boolean isCheckedItemRulePostEdit = driver.findElement(By.id("id_rules_0")).isSelected();
        assertTrue(isCheckedItemRulePostEdit);

        System.out.println("Deleting Item....");

        driver.findElement(By.cssSelector("div[name='delete']")).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("delete"))).click();

        menuAppPO.acceptAlert();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
