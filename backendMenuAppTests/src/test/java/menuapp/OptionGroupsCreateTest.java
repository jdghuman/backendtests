package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class OptionGroupsCreateTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Test
    public void createOptionGroupLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Check if Option Group exists
        List<WebElement> testOptGrpBefore = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpBefore.size());

        // Enter Option Group Name
        driver.findElement(By.id("id_form-0-name")).sendKeys(menuAppPO.optionGroupName);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        // Verify Option Group is added
        List<WebElement> testOptGrpAfter = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpAfter.size());
        if (testOptGrpAfter.size() > testOptGrpBefore.size()) {
            // do nothing - Option Group added
        } else throw new Exception("Option Group not added!");

        // Select Option Group
        driver.findElement(By.linkText(menuAppPO.optionGroupName)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Option Group"));

        menuAppPO.deleteOptionGroup();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Option Group"));

        // Verify Option Group is added
        List<WebElement> testOptGrpDelete = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpDelete.size());
        if (testOptGrpDelete.size() < testOptGrpAfter.size()) {
            // do nothing - Option Group added
        } else throw new Exception("Option Group not deleted!");
    }

    @Test
    public void createOptionGroupAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Verify Admin Location
        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        // Check if Option Group exists
        List<WebElement> testOptGrpBefore = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpBefore.size());

        // Enter Option Group Name
        driver.findElement(By.id("id_form-0-name")).sendKeys(menuAppPO.optionGroupName);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        // Verify Option Group is added
        List<WebElement> testOptGrpAfter = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpAfter.size());
        if (testOptGrpAfter.size() > testOptGrpBefore.size()) {
            // do nothing - Option Group added
        } else throw new Exception("Option Group not added!");

        // Select Option Group
        driver.findElement(By.linkText(menuAppPO.optionGroupName)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Option Group"));

        menuAppPO.deleteOptionGroup();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Option Group"));

        // Verify Option Group is added
        List<WebElement> testOptGrpDelete = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpDelete.size());
        if (testOptGrpDelete.size() < testOptGrpAfter.size()) {
            // do nothing - Option Group added
        } else throw new Exception("Option Group not deleted!");
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
