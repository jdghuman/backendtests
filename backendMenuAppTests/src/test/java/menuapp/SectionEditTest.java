package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class SectionEditTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;


    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Test
    public void editSectionLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.storeMenu));

        menuAppPO.selectAddPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Page"));

        // Enter Section Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.sectionName);

        // Verify Section is enabled - Check if not enabled
        Boolean isEnabledAddPage = driver.findElement(By.id("id_enabled")).isSelected();
        if (isEnabledAddPage) {
            // Section is enabled
        } else {
            driver.findElement(By.id("id_enabled")).click();
        }

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Wait for page refresh
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.sectionName));

        // Verify Section appears in the list
        List<WebElement> testMenuAfter = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Store: " + testMenuAfter.size());
        if (testMenuAfter.size() > 0) {
            // do nothing - Section added correctly
        } else throw new Exception("Section is not added!");

        // Select Section
        testMenuAfter.get(testMenuAfter.size() -1).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Edit Page")));

        menuAppPO.selectEditPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Page"));

        menuAppPO.selectRulesDropdown();

        // Check if checkbox is checked
        Boolean isCheckedAllDayVis = driver.findElement(By.id("id_rules_0")).isSelected();

        if (!isCheckedAllDayVis) {
            // Check checkbox
            driver.findElement(By.id("id_rules_0")).click();
        } else throw new Exception("Checkbox is checked!");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Wait for page refresh
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.sectionName));

        menuAppPO.selectEditPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Page"));

        menuAppPO.selectRulesDropdown();

        // Check if checkbox is checked
        Boolean isCheckedAllDayVisPost = driver.findElement(By.id("id_rules_0")).isSelected();

        // Verify Section edit saved successfully
        if (isCheckedAllDayVisPost) {
            // do nothing - Section edit saved
        } else throw new Exception("Section edit not saved!");

        System.out.println("Deleting Section....");

        menuAppPO.selectDeletePage();

        menuAppPO.acceptAlert();
    }

    @Test
    public void editSectionAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectMenus();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.corpMenu));

        // Verify Admin Location
        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectAddPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Page"));

        // Enter Section Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.sectionName);

        // Verify Section is enabled - Check if not enabled
        Boolean isEnabledAddPage = driver.findElement(By.id("id_enabled")).isSelected();
        if (isEnabledAddPage) {
            // Menu Section is enabled
        } else {
            driver.findElement(By.id("id_enabled")).click();
        }

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Wait for page refresh
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.sectionName));

        // Verify Section appears in the list
        List<WebElement> testMenuAfter = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println(testMenuAfter.size());
        if (testMenuAfter.size() > 0) {
            // do nothing - Section added correctly
        } else throw new Exception("Section is not added!");

        // Select Section
        testMenuAfter.get(testMenuAfter.size() -1).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Edit Page")));

        menuAppPO.selectEditPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Page"));

        menuAppPO.selectRulesDropdown();

        // Check if checkbox is checked
        Boolean isCheckedAllDayVis = driver.findElement(By.id("id_rules_0")).isSelected();

        if (!isCheckedAllDayVis) {
            // Check checkbox
            driver.findElement(By.id("id_rules_0")).click();
        } else throw new Exception("Checkbox is checked!");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Wait for page refresh
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.sectionName));

        menuAppPO.selectEditPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Page"));

        menuAppPO.selectRulesDropdown();

        // Check if checkbox is checked
        Boolean isCheckedAllDayVisPost = driver.findElement(By.id("id_rules_0")).isSelected();

        // Verify Section edit saved successfully
        if (isCheckedAllDayVisPost) {
            // do nothing - Section edit saved
        } else throw new Exception("Section edit not saved!");

        System.out.println("Deleting Section....");

        menuAppPO.selectDeletePage();

        menuAppPO.acceptAlert();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
