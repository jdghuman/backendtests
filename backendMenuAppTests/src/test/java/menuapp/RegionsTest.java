package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RegionsTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Test
    public void addRegion() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectRegionsFromNavBar();

        // Verify Region count
        List<WebElement> regionBefore = driver.findElements(By.partialLinkText("TestRegion"));
        System.out.println("RegionNameBefore: " + regionBefore.size());

        menuAppPO.selectStoreDisplayDropdown();

        int count = 0;
        List<WebElement> linkList = driver.findElements(By.tagName("a"));

        for(int i=0 ; i<linkList.size() ; i++) {

            if (linkList.get(i).getAttribute("href").contains("/region")) {
                count++;
            }
        }
        System.out.println("Count: " + count);

        menuAppPO.selectStoreDisplayDropdown();

        menuAppPO.selectRegionsFromNavBar();

        menuAppPO.selectAddButton();

        menuAppPO.addRegionName();

        // Region name entered
        String regionNameEntered = driver.findElement(By.id("id_name")).getAttribute("value");
        System.out.println("\nRegion Name Created: " + regionNameEntered + "\n");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Verify Region is added
        List<WebElement> regionAfter = driver.findElements(By.partialLinkText("TestRegion"));
        System.out.println("RegionNameAfter: " + regionAfter.size());
        if (regionAfter.size() > regionBefore.size()) {
            // do nothing - Region added
        } else throw new Exception("Region not added!");

        menuAppPO.selectStoreDisplayDropdown();

        int countTwo = 0;
        List<WebElement> linkListTwo = driver.findElements(By.tagName("a"));

        for(int i=0 ; i<linkListTwo.size() ; i++) {

            if (linkListTwo.get(i).getAttribute("href").contains("/region")) {
                countTwo++;
            }
        }

        System.out.println("CountTwo: " + countTwo);

        menuAppPO.selectStoreDisplayDropdown();

        menuAppPO.deleteRegion();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
