package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;


public class CancelButtonTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }


    @Test
    public void cancelButtonOnMenuItem() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Check if Item exists
        List<WebElement> testItemBefore = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        System.out.println("Item Before: " + testItemBefore.size());

        menuAppPO.selectAddItem();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Section: " + menuAppPO.section));

        menuAppPO.enterItemName();

        Thread.sleep(500);

        menuAppPO.selectCancelButton();

        menuAppPO.acceptAlert();

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is NOT added
        List<WebElement> testItemAfter = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        // List<WebElement> testItemAfter = driver.findElements(By.className(menuAppPO.sectionName));
        System.out.println("Item After: " + testItemAfter.size());

        if (testItemAfter.size() == testItemBefore.size()) {
            // do nothing - item Cancelled successfully
        } else throw new Exception("Item add did not cancel!");

    }

    @Test
    public void cancelButtonOnSection() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for "+menuAppPO.location+".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        // Check if Section already exists
        List<WebElement> testMenuBefore = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Section: " + testMenuBefore.size());

        menuAppPO.selectAddPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Page"));

        menuAppPO.enterSectionName();

        menuAppPO.selectCancelButton();

        // Verify Section does NOT appear
        List<WebElement> testMenuAfter = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Section: " + testMenuAfter.size());
        if (testMenuAfter.size() == testMenuBefore.size()) {
            // do nothing - Section cancelled successfully
        } else throw new Exception("Section not cancelled!");
    }

    @Test
    public void cancelButtonOptionGroups() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Check if Option Group exists
        List<WebElement> testOptGrpBefore = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpBefore.size());

        menuAppPO.enterOptionGroupName();

        menuAppPO.selectCancel();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("welcomeh1")));

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Check if Option Group exists
        List<WebElement> testOptGrpAfter = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpAfter.size());

        if (testOptGrpAfter.size() == testOptGrpBefore.size()) {
            // do nothing - Option Group cancelled successfully
        } else throw new Exception("Option Group not cancelled!");
    }


    @After
    public void tearDown() {
        driver.quit();
    }
}
