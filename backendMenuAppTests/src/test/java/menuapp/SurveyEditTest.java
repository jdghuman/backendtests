package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class SurveyEditTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;


    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Test
    public void editSurvey() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectAnalyticsFromNavBar();

        menuAppPO.selectSurveyFromDashboard();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Surveys"));

        List<WebElement> testSurveyBefore = driver.findElements(By.linkText(menuAppPO.survey));
        System.out.println("SurveyBefore: " + testSurveyBefore.size());

        // Tap 'Add'
        driver.findElement(By.linkText("Add")).click();

        // Enter Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.survey);

        // Enter Description
        driver.findElement(By.id("id_questions-0-description")).sendKeys("test1");

        // Tap 'Save Changes'
        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        // Verify Survey is added
        List<WebElement> testSurveyAfter = driver.findElements(By.linkText(menuAppPO.survey));
        System.out.println("SurveyAfter: " + testSurveyAfter.size());
        if (testSurveyAfter.size() > testSurveyBefore.size()) {
            // do nothing - Survey added
        } else throw new Exception("Survey not added!");

        ////

        // Select Survey
        driver.findElement(By.linkText(menuAppPO.survey)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.survey));

        // Select 'Edit Survey'
        driver.findElement(By.xpath("//*[contains(text(),'Edit Survey')]")).click();

        // Edit Name
        driver.findElement(By.id("id_name")).sendKeys("2");

        // Edit Description
        driver.findElement(By.id("id_questions-0-description")).sendKeys("2");

        // Tap 'Save Changes'
        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        // Select 'Edit Survey'
        driver.findElement(By.xpath("//*[contains(text(),'Edit Survey')]")).click();

        // Verify Name Edits appear
        assertTrue(driver.findElement(By.id("id_name")).getAttribute("value").equalsIgnoreCase(menuAppPO.survey+"2"));

        // Verify Description Edits appear
        assertTrue(driver.findElement(By.id("id_questions-0-description")).getAttribute("value").equalsIgnoreCase("test12"));

        menuAppPO.deleteSurvey();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Surveys"));

        // Verify Survey is deleted
        List<WebElement> testSurveyDelete = driver.findElements(By.linkText(menuAppPO.survey));
        System.out.println("SurveyDelete: " + testSurveyDelete.size());
        if (testSurveyDelete.size() < testSurveyAfter.size()) {
            // do nothing - Survey Deleted
        } else throw new Exception("Survey not deleted!");
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
