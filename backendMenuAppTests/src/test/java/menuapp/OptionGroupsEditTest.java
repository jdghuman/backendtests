package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OptionGroupsEditTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Test
    public void editOptionGroupLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Check if Option Group exists
        List<WebElement> testOptGrpBefore = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpBefore.size());

        // Enter Option Group Name
        driver.findElement(By.id("id_form-0-name")).sendKeys(menuAppPO.optionGroupName);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        // Verify Option Group is added
        List<WebElement> testOptGrpAfter = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpAfter.size());
        if (testOptGrpAfter.size() > testOptGrpBefore.size()) {
            // do nothing - Option Group added
        } else throw new Exception("Option Group not added!");

        // Select Option Group
        testOptGrpAfter.get(testOptGrpAfter.size() -1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Option Group"));

        // Edit 'Minimum Required'
        driver.findElement(By.id("id_form-0-selection_min")).sendKeys(Keys.chord(Keys.COMMAND, "a"), "1");

        // Edit 'Maximum Allowed'
        driver.findElement(By.id("id_form-0-selection_max")).sendKeys(Keys.chord(Keys.COMMAND, "a"), "1");

        // Edit Modifier 'Name'
        driver.findElement(By.id("id_subform-0-0-name")).sendKeys("option");

        // Edit 'POS Code'
        driver.findElement(By.id("id_subform-0-0-pos_code")).sendKeys("no_modifier");

        // Edit Modifier Price
        driver.findElement(By.id("id_subform-0-0-price")).sendKeys("2.00");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Verify Option Group appears
        List<WebElement> optionGroupAfterTwo = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("Rule: " + optionGroupAfterTwo.size());

        // Select Option Group
        optionGroupAfterTwo.get(optionGroupAfterTwo.size() -1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Option Group"));

        /////////////// Verify Edits Saved ///////////////////

        // Verify Minimum Required value
        String minVal = driver.findElement(By.id("id_form-0-selection_min")).getAttribute("value");
        assertTrue(minVal.equalsIgnoreCase("1"));

        // Verify Maximum allowed value
        String maxVal = driver.findElement(By.id("id_form-0-selection_max")).getAttribute("value");
        assertTrue(maxVal.equalsIgnoreCase("1"));

        // Verify Modifier Name saved
        assertEquals("Modifier name not saved!", "option", driver.findElement(By.id("id_subform-0-0-name")).getAttribute("value"));

        // Verify Edit POS code
        assertEquals("Option Group POS code not saved!", "no_modifier", driver.findElement(By.id("id_subform-0-0-pos_code")).getAttribute("value"));

        // Verify Modifier Price
        assertEquals("Option Group Modifier price not saved!", "2.00", driver.findElement(By.id("id_subform-0-0-price")).getAttribute("value"));

        menuAppPO.deleteOptionGroup();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Option Group"));
    }

    @Test
    public void editOptionGroupAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Check if Option Group exists
        List<WebElement> testOptGrpBefore = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpBefore.size());

        // Enter Option Group Name
        driver.findElement(By.id("id_form-0-name")).sendKeys(menuAppPO.optionGroupName);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        // Verify Option Group is added
        List<WebElement> testOptGrpAfter = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("OptGrp: " + testOptGrpAfter.size());
        if (testOptGrpAfter.size() > testOptGrpBefore.size()) {
            // do nothing - Option Group added
        } else throw new Exception("Option Group not added!");

        // Select Option Group
        testOptGrpAfter.get(testOptGrpAfter.size() -1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Option Group"));

        // Edit 'Minimum Required'
        driver.findElement(By.id("id_form-0-selection_min")).sendKeys(Keys.chord(Keys.COMMAND, "a"), "1");

        // Edit 'Maximum Allowed'
        driver.findElement(By.id("id_form-0-selection_max")).sendKeys(Keys.chord(Keys.COMMAND, "a"), "1");

        // Edit Modifier 'Name'
        driver.findElement(By.id("id_subform-0-0-name")).sendKeys("option");

        // Edit 'POS Code'
        driver.findElement(By.id("id_subform-0-0-pos_code")).sendKeys("no_modifier");

        // Edit Modifier Price
        driver.findElement(By.id("id_subform-0-0-price")).sendKeys("2.00");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectOptionGroupsFromNavBar();

        menuAppPO.verifyOptionGroupsPageLoads();

        // Verify Option Group appears
        List<WebElement> optionGroupAfterTwo = driver.findElements(By.linkText(menuAppPO.optionGroupName));
        System.out.println("Rule: " + optionGroupAfterTwo.size());

        // Select Option Group
        optionGroupAfterTwo.get(optionGroupAfterTwo.size() -1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Option Group"));

        /////////////// Verify Edits Saved ///////////////////

        // Verify Minimum Required value
        String minVal = driver.findElement(By.id("id_form-0-selection_min")).getAttribute("value");
        assertTrue(minVal.equalsIgnoreCase("1"));

        // Verify Maximum allowed value
        String maxVal = driver.findElement(By.id("id_form-0-selection_max")).getAttribute("value");
        assertTrue(maxVal.equalsIgnoreCase("1"));

        // Verify Modifier Name saved
        assertEquals("Modifier name not saved!", "option", driver.findElement(By.id("id_subform-0-0-name")).getAttribute("value"));

        // Verify Edit POS code
        assertEquals("Option Group POS code not saved!", "no_modifier", driver.findElement(By.id("id_subform-0-0-pos_code")).getAttribute("value"));

        // Verify Modifier Price
        assertEquals("Option Group Modifier price not saved!", "2.00", driver.findElement(By.id("id_subform-0-0-price")).getAttribute("value"));

        menuAppPO.deleteOptionGroup();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Option Group"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
