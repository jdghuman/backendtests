package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RulesEditTest {

    MenuAppPO menuAppPO;
    public WebDriverWait wait;
    public WebDriver driver;
    WebElement multSelectOption;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Ignore
    @Test
    public void editRulesLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        List<WebElement> rulebefore = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + rulebefore.size());

        // Enter Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.rule);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        // Verify Rule appears
        List<WebElement> ruleAfter = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + ruleAfter.size());
        if (ruleAfter.size() > rulebefore.size()) {
            // do nothing - rule added
        } else throw new Exception("Rule not added!");

        // Select Rule
        ruleAfter.get(ruleAfter.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Rule"));

        menuAppPO.expandRulesAvailability();

        // Select > Is Available > 'Yes'
        multSelectOption = driver.findElement(By.cssSelector("select[id='id_is_available']"));
        Select clickM = new Select(multSelectOption);

        // Select Option Value
        clickM.selectByVisibleText("Yes");

        menuAppPO.expandRulesPriceAndVisibility();

        menuAppPO.selectSundayRule();

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        // Verify Rule appears
        List<WebElement> ruleAfterEdits = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + ruleAfterEdits.size());

        // Select Rule
        ruleAfterEdits.get(ruleAfterEdits.size() -1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Rule"));

        menuAppPO.expandRulesAvailability();

        // Verify Edit (Is Available > 'Yes')
        Select selectedValue = new Select(driver.findElement(By.id("id_is_available")));
        WebElement optionSelected = selectedValue.getFirstSelectedOption();
        assertTrue(optionSelected.getText().equalsIgnoreCase("yes"));

        menuAppPO.expandRulesPriceAndVisibility();

        // Verify 'Sun' is selected
        assertTrue(driver.findElement(By.id("id_sunday")).isSelected());

        menuAppPO.selectDeleteRule();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));
    }

    @Test
    public void editRulesAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        // Verify Admin Location
        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        // Check if Rule appears
        List<WebElement> rulebefore = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + rulebefore.size());

        // Enter Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.rule);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        // Verify Rule appears
        List<WebElement> ruleAfter = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + ruleAfter.size());
        if (ruleAfter.size() > rulebefore.size()) {
            // do nothing - rule added
        } else throw new Exception("Rule not added!");

        // Select Rule
        ruleAfter.get(ruleAfter.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Rule"));

        menuAppPO.expandRulesAvailability();

        // Select > Is Available > 'Yes'
        multSelectOption = driver.findElement(By.cssSelector("select[id='id_is_available']"));
        Select clickM = new Select(multSelectOption);

        // Select Option Value
        clickM.selectByVisibleText("Yes");

        menuAppPO.expandRulesPriceAndVisibility();

        menuAppPO.selectSundayRule();

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        // Verify Rule appears
        List<WebElement> ruleAfterEdits = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + ruleAfterEdits.size());

        // Select Rule
        ruleAfterEdits.get(ruleAfterEdits.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Rule"));

        menuAppPO.expandRulesAvailability();

        // Verify Edit (Is Available > 'Yes')
        Select selectedValue = new Select(driver.findElement(By.id("id_is_available")));
        WebElement optionSelected = selectedValue.getFirstSelectedOption();
        assertTrue("Rule setting not correct", optionSelected.getText().equalsIgnoreCase("yes"));

        menuAppPO.expandRulesPriceAndVisibility();

        menuAppPO.selectSundayRule();

        menuAppPO.selectDeleteRule();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
