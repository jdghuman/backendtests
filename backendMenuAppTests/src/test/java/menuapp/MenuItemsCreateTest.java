package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class MenuItemsCreateTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
    }

    @Test
    public void createMenuItemLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for "+menuAppPO.location+".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Check if Item already exists
        List<WebElement> testItemBefore = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        System.out.println("Item: " + testItemBefore.size());

        menuAppPO.selectAddItem();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Section: " + menuAppPO.section));

        // Enter Item Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.item);

        // Enter Short Description
        driver.findElement(By.id("id_short_desc")).sendKeys("Short Descrip Test");

        // Enter Long Description
        driver.findElement(By.id("id_long_desc")).sendKeys("Long Descript Test");

        // Enter Price Level 1
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys("2.00");

        // Enter POS code
        driver.findElement(By.id("id_pos_code")).sendKeys("1234");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        // Tap Section in breadcrumb
        driver.findElement(By.linkText(menuAppPO.section)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfter = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        // List<WebElement> testItemAfter = driver.findElements(By.className(menuAppPO.sectionName));
        System.out.println("Item: " + testItemAfter.size());

        if (testItemAfter.size() > testItemBefore.size()) {
            // do nothing - item added successfully
        } else throw new Exception("Item is not added!");

        System.out.println("Deleting Item....");

        // Select Item
        testItemAfter.get(testItemAfter.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.item));

        menuAppPO.selectDeleteItem();

        menuAppPO.selectDeleteButton();

        menuAppPO.acceptAlert();
    }

    @Test
    public void createMenuItemAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectMenus();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.corpMenu));

        // Verify Admin Location
        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Check if Item already exists
        List<WebElement> testItemBefore = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        System.out.println("Item: " + testItemBefore.size());

        menuAppPO.selectAddItem();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Section: " + menuAppPO.section));

        // Enter Item Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.item);

        // Enter Short Description
        driver.findElement(By.id("id_short_desc")).sendKeys("Short Descrip Test");

        // Enter Long Description
        driver.findElement(By.id("id_long_desc")).sendKeys("Long Descript Test");

        // Enter Price Level 1
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys("2.00");

        // Enter POS code
        driver.findElement(By.id("id_pos_code")).sendKeys("1234");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        // Tap Section in breadcrumb
        driver.findElement(By.linkText(menuAppPO.section)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfter = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        // List<WebElement> testItemAfter = driver.findElements(By.className(menuAppPO.sectionName));
        System.out.println("Item: " + testItemAfter.size());

        if (testItemAfter.size() > testItemBefore.size()) {
            // do nothing - item added successfully
        } else throw new Exception("Item is not added!");

        System.out.println("Deleting Item....");

        // Select Item
        testItemAfter.get(testItemAfter.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.item));

        menuAppPO.selectDeleteItem();

        menuAppPO.selectDeleteButton();

        menuAppPO.acceptAlert();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
