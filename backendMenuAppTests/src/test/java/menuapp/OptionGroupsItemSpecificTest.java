package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OptionGroupsItemSpecificTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    // Food options tab within a menu
    @Test
    public void editOptionGroupsItemSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for "+menuAppPO.location+".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Check if Item already exists
        List<WebElement> testItemBefore = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        System.out.println("Item: " + testItemBefore.size());

        menuAppPO.selectAddItem();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Section: " + menuAppPO.section));

        // Enter Item Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.item);

        // Enter Short Description
        driver.findElement(By.id("id_short_desc")).sendKeys("Short Descrip Test");

        // Enter Long Description
        driver.findElement(By.id("id_long_desc")).sendKeys("Long Descript Test");

        // Enter Price Level 1
        driver.findElement(By.id("id_price")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.id("id_price")).sendKeys("2.00");

        // Enter POS code
        driver.findElement(By.id("id_pos_code")).sendKeys("1234");

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Tap Section in breadcrumb
        driver.findElement(By.linkText(menuAppPO.section)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfter = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        // List<WebElement> testItemAfter = driver.findElements(By.className(menuAppPO.sectionName));
        System.out.println("Item: " + testItemAfter.size());

        if (testItemAfter.size() > testItemBefore.size()) {
            // do nothing - item added successfully
        } else throw new Exception("Item is not added!");

        // Select Item
        testItemAfter.get(testItemAfter.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.item));

        menuAppPO.selectFoodOptions();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_option_group")));

        menuAppPO.selectAddOptionGroup();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_form-0-name")));

        // Enter Option Group Name
        driver.findElement(By.id("id_form-0-name")).sendKeys(menuAppPO.optionGroupName);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectMenus();

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfterOG = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        System.out.println("OG: " + testItemAfterOG.size());

        // Select Item
        testItemAfterOG.get(testItemAfterOG.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.item));

        menuAppPO.selectFoodOptions();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_option_group")));

        // Edit 1st Option Group

        // Edit 'Minimum Required'
        driver.findElement(By.id("id_form-0-selection_min")).sendKeys(Keys.chord(Keys.COMMAND, "a"), "1");

        // Edit 'Maximum Allowed'
        driver.findElement(By.id("id_form-0-selection_max")).sendKeys(Keys.chord(Keys.COMMAND, "a"), "1");

        // Enter Modifier Name
        driver.findElement(By.id("id_subform-0-0-name")).sendKeys(menuAppPO.modName);

        // Enter POS Code
        driver.findElement(By.id("id_subform-0-0-pos_code")).sendKeys("no_modifier");

        // Enter Modifier Price
        driver.findElement(By.id("id_subform-0-0-price")).sendKeys("2.00");

        // Verify Option Group saved
        assertEquals(menuAppPO.optionGroupName, driver.findElement(By.id("id_form-0-name")).getAttribute("value"));

        // Select Option Group from dropdown
        Select optGroupDrpdn = new Select(driver.findElement(By.id("id_option_group")));
        optGroupDrpdn.selectByVisibleText("Add a soup or salad");

        menuAppPO.selectApplyOptionGroup();

        Thread.sleep(500);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(2000);

        // Verify Option Group 1 & 2 edits saved

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectMenus();

        menuAppPO.verifyMenuPageLoads();

        menuAppPO.selectPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.page));

        menuAppPO.selectSection();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.section));

        // Verify Item is added
        List<WebElement> testItemAfterOGTwo = driver.findElements(By.xpath("//*[contains(text(), 'TestItem')]"));
        System.out.println("OG: " + testItemAfterOGTwo.size());

        // Select Item
        testItemAfterOGTwo.get(testItemAfterOGTwo.size() - 1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("current-name"), menuAppPO.item));

        menuAppPO.selectFoodOptions();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_option_group")));

        // Option Group - 1

        // Verify Minimum Required value
        String minVal = driver.findElement(By.id("id_form-0-selection_min")).getAttribute("value");
        assertTrue(minVal.equalsIgnoreCase("1"));

        // Verify Maximum allowed value
        String maxVal = driver.findElement(By.id("id_form-0-selection_max")).getAttribute("value");
        assertTrue(maxVal.equalsIgnoreCase("1"));

        // Verify Modifier Name saved
        assertEquals("Modifier name not saved!", menuAppPO.modName, driver.findElement(By.id("id_subform-0-0-name")).getAttribute("value"));

        // Verify Edit POS code
        assertEquals("Option Group POS code not saved!", "no_modifier", driver.findElement(By.id("id_subform-0-0-pos_code")).getAttribute("value"));

        // Verify Modifier Price
        assertEquals("Option Group Modifier price not saved!", "2.00", driver.findElement(By.id("id_subform-0-0-price")).getAttribute("value"));

        // Option Group - 2
        assertEquals("Add a soup or salad", driver.findElement(By.id("id_form-1-name")).getAttribute("value"));

        System.out.println("Deleting Item....");

        menuAppPO.selectDeleteItem();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("delete"))).click();

        menuAppPO.acceptAlert();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
