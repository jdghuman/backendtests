package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class LocationsTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
    }

    @Test
    public void verifyLocationTabsLinksAreWorking() throws IOException, InterruptedException {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectLocationsFromNavBar();

        menuAppPO.verifyLocationsPageLoads();

        // Select location
        driver.findElement(By.linkText(menuAppPO.location)).click();

        // Verify Locations settings page loads
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Location: " + menuAppPO.location));

        // Verify 'Display Tab' loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_name")));

        // Select 'iPad Settings'
        driver.findElement(By.cssSelector("div[data-form='ipad-settings']")).click();

        // Verify 'iPad Settings' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_promo_pages")));

        // Select 'Payment Settings'
        driver.findElement(By.cssSelector("div[data-form='payment-settings']")).click();

        // Verify 'Payment Settings' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_view_check_inactivity_trigger_seconds_table_mode")));

        // Select 'Receipt Settings'
        driver.findElement(By.cssSelector("div[data-form='receipt-settings']")).click();

        // Verify 'Receipt Settings' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_receipt_price_options")));

        // Select 'Tax Settings'
        driver.findElement(By.cssSelector("div[data-form='tax-settings']")).click();

        // Verify 'Tax Settings' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_tax-rate-0-description")));

        // Select 'Syncing'
        driver.findElement(By.cssSelector("div[data-form='syncing']")).click();

        // Verify 'Syncing' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_auto_update_config_upon_device_wake_up")));

        // Select 'Chit Controls'
        driver.findElement(By.cssSelector("div[data-form='chit-controls']")).click();

        // Verify 'Chit Controls' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_chit_price_options")));

        // Select 'Revenue Centers'
        driver.findElement(By.cssSelector("div[data-form='revenue-centers']")).click();

        // Verify 'Revenue Centers'
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_honor_server_revenue_center_associations")));

        // Select 'Server Mode'
        driver.findElement(By.cssSelector("div[data-form='server-mode']")).click();

        // Verify 'Server Mode' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_enable_option_group_special_instructions_server")));

        // Select 'Kiosk Mode'
        driver.findElement(By.cssSelector("div[data-form='kiosk-mode']")).click();

        // Verify 'Kiosk Mode' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_enable_option_group_special_instructions_kiosk")));

        // Select 'Other Modes'
        driver.findElement(By.cssSelector("div[data-form='other-modes']")).click();

        // Verify 'Other Modes' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_enable_option_group_special_instructions_table")));

        // Select 'Staff Pins'
        driver.findElement(By.cssSelector("div[data-form='staff-pins']")).click();

        // Verify 'Staff Pins' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_pin-0-name")));

        // Select 'Connected POS'
        driver.findElement(By.cssSelector("div[data-form='connected-pos']")).click();

        // Verify 'Connected POS' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("test_pos")));

        // Select 'Print Devices'
        driver.findElement(By.cssSelector("div[data-form='print-devices']")).click();

        // Verify 'Print Devices' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_print_devices-0-name")));

        // Select 'Device Requests'
        driver.findElement(By.cssSelector("div[data-form='device-requests']")).click();

        // Verify 'Device Requests' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_device_requests-0-name")));

        // Select 'Loyalty Program'
        driver.findElement(By.cssSelector("div[data-form='loyalty-program']")).click();

        // Verify 'Loyalty Program' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_loyalty_vendor")));

        // Select 'Third Party'
        driver.findElement(By.cssSelector("div[data-form='third-party']")).click();

        // Verify 'Third Party' tab loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_pay_from_ipad")));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
