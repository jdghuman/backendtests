package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class SectionCreateTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Test
    public void addSectionLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        // Verify Sections in Store Menu
        List<WebElement> testMenuBeforeStore = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Store: " + testMenuBeforeStore.size());

        // Select Corp Menu
        driver.findElement(By.linkText(menuAppPO.corpMenu)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.corpMenu));

        // Verify Section in the Corp Menu
        List<WebElement> testMenuCorpBefore = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Corp: " + testMenuCorpBefore.size());

        // Select Store Menu
        driver.findElement(By.linkText(menuAppPO.storeMenu)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.storeMenu));

        // Select 'Add Page'
        driver.findElement(By.id("add-item-button")).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Page"));

        // Enter Section Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.sectionName);

        // Verify Section is enabled - Check if not enabled
        Boolean isEnabledAddPage = driver.findElement(By.id("id_enabled")).isSelected();
        if (isEnabledAddPage) {
            // Section is enabled
        } else {
            driver.findElement(By.id("id_enabled")).click();
        }

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Wait for page refresh
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.sectionName));

        // Verify Section appears in the list
        List<WebElement> testMenuAfterStore = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Store: " + testMenuAfterStore.size());
        if (testMenuBeforeStore.size() <= testMenuAfterStore.size()) {
            // do nothing - Section added correctly
        } else throw new Exception("Section is not added!");

        // Navigate to Menus
        menuAppPO.selectMenuManagerLinkInNavBar();
        menuAppPO.selectMenus();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Menus"));

        // Select Corp Menu
        driver.findElement(By.linkText(menuAppPO.corpMenu)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.corpMenu));

        // Verify Store Section does NOT appear in the Corp Menu
        List<WebElement> testMenuCorpAfter = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Corp: " + testMenuCorpAfter.size());
        if (testMenuCorpBefore.size() == testMenuCorpAfter.size()) {
            // do nothing - Section not added to Corp Menu
        } else throw new Exception("Store Section added to Corp Menu!");

        // Select Store Menu
        driver.findElement(By.linkText(menuAppPO.storeMenu)).click();

        // Wait for Store Menu to load
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("page-header"), menuAppPO.storeMenu));

        System.out.println("Deleting Section....");

        // Select Section
        driver.findElement(By.partialLinkText(menuAppPO.sectionName)).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Edit Page")));

        menuAppPO.selectEditPage();

        // Select 'Delete Page'
        wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Delete Page")));

        menuAppPO.selectDeletePage();

        menuAppPO.acceptAlert();

        // Check if Section title exists
        List<WebElement> testMenuAfterDelete = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println(testMenuAfterDelete.size());
    }

    @Test
    public void addSectionAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for "+menuAppPO.location+".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        // Verify Sections in Store Menu
        List<WebElement> testMenuBeforeStore = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Store: " + testMenuBeforeStore.size());

        menuAppPO.selectAdminLocation();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.corpMenu));

        // Verify Admin Location
        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        // Check if Section already exists in Corp Menu
        List<WebElement> testMenuBeforeCorp = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Corp: " + testMenuBeforeCorp.size());

        menuAppPO.selectAddPage();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Page"));

        // Enter Section Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.sectionName);

        // Verify Section is enabled - Check if not enabled
        Boolean isEnabledAddPage = driver.findElement(By.id("id_enabled")).isSelected();
        if (isEnabledAddPage) {
            // Section is enabled
        } else {
            driver.findElement(By.id("id_enabled")).click();
        }

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Wait for page refresh
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.sectionName));

        // Verify Section appears in the list - Corp Menu
        List<WebElement> testMenuAfterCorp = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Corp After: " +testMenuAfterCorp.size());
        if (testMenuBeforeCorp.size() <= testMenuAfterCorp.size()) {
            // do nothing - Section added correctly
        } else throw new Exception("Section is not added!");

        // Navigate to Store Menu
        menuAppPO.selectStoreDisplayDropdown();

        // Select Location
        driver.findElement(By.linkText(menuAppPO.location)).click();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.verifyMenuPageLoads();

        // Select Store Menu
        driver.findElement(By.linkText(menuAppPO.storeMenu)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.storeMenu));

        // Verify Section appears in the list - Store Menu
        List<WebElement> testMenuAfterStore = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println("Store After: " +testMenuAfterStore.size());
        if (testMenuBeforeStore.size() == testMenuAfterStore.size()) {
            // do nothing - Section added correctly
        } else throw new Exception("Admin Section is added to the Store Menu!");

        System.out.println("Deleting Section....");

        menuAppPO.selectStoreDisplayDropdown();

        // Select Admin
        driver.findElement(By.linkText(menuAppPO.admin)).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), menuAppPO.corpMenu));

        // Select Section
        driver.findElement(By.linkText(menuAppPO.sectionName)).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Edit Page")));

        menuAppPO.selectEditPage();

        menuAppPO.selectDeletePage();

        menuAppPO.acceptAlert();

        // Check if Section title exists
        List<WebElement> testMenuAfterDelete = driver.findElements(By.partialLinkText(menuAppPO.sectionName));
        System.out.println(testMenuAfterDelete.size());
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
