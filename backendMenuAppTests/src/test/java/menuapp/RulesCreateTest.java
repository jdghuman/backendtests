package menuapp;

import com.MenuAppPO;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class RulesCreateTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
        // driver.manage().window().maximize();
    }

    @Ignore
    @Test
    public void createRuleLocationSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectStoreLocation();

        // Verify Location
        assertEquals("You are editing MenuPad settings/data for " + menuAppPO.location + ".", driver.findElement(By.className("info-banner")).getText());
        System.out.println(driver.findElement(By.className("info-banner")).getText());

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        List<WebElement> rulebefore = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + rulebefore.size());

        // Enter Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.rule);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Verify Rule appears
        List<WebElement> ruleAfter = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + ruleAfter.size());
        if (ruleAfter.size() > rulebefore.size()) {
            // do nothing - rule added
        } else throw new Exception("Rule not added!");

        // Select Rule
        ruleAfter.get(ruleAfter.size() -1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Rule"));

        menuAppPO.selectDeleteRule();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));
    }

    @Test
    public void createRuleAdminSpecific() throws Exception {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectMenuManagerLinkInNavBar();

        menuAppPO.selectRules();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));

        List<WebElement> rulebefore = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + rulebefore.size());

        // Enter Name
        driver.findElement(By.id("id_name")).sendKeys(menuAppPO.rule);

        menuAppPO.selectSave();

        menuAppPO.verifyYourChangesHaveBeenSavedMessage();

        Thread.sleep(3000);

        // Verify Rule appears
        List<WebElement> ruleAfter = driver.findElements(By.linkText(menuAppPO.rule));
        System.out.println("Rule: " + ruleAfter.size());
        if (ruleAfter.size() > rulebefore.size()) {
            // do nothing - rule added
        } else throw new Exception("Rule not added!");

        // Select Rule
        ruleAfter.get(ruleAfter.size() -1).click();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Rule"));

        menuAppPO.selectDeleteRule();

        menuAppPO.acceptAlert();

        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Rule"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
