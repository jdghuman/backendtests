package menuapp;

import com.MenuAppPO;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class DisplayDetailsTest {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
    }

    @Test
    public void displayDetails() throws IOException {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.selectLocationsFromNavBar();

        menuAppPO.verifyLocationsPageLoads();

        // Select location
        driver.findElement(By.linkText(menuAppPO.location)).click();

        // Verify Locations settings page loads
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Edit Location: " + menuAppPO.location));

        // Verify 'Display Tab' loads
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_name")));

        String oName = driver.findElement(By.id("id_name")).getAttribute("value");
        String oNumber = driver.findElement(By.id("id_restaurant_id")).getAttribute("value");
        String oShortDesc = driver.findElement(By.id("id_short_desc")).getAttribute("value");
        String oLongDesc = driver.findElement(By.id("id_long_desc")).getAttribute("value");
        String oAddress1 = driver.findElement(By.id("id_address_1")).getAttribute("value");
        String oAddres2 = driver.findElement(By.id("id_address_2")).getAttribute("value");
        String oCity = driver.findElement(By.id("id_city")).getAttribute("value");
        String oState = driver.findElement(By.id("id_state")).getAttribute("value");
        String oZip = driver.findElement(By.id("id_zipcode")).getAttribute("value");
        String oCountry = driver.findElement(By.id("id_country")).getAttribute("value");
        String oPhone = driver.findElement(By.id("id_phone")).getAttribute("value");
        String oFax = driver.findElement(By.id("id_fax")).getAttribute("value");
        String oEmail = driver.findElement(By.id("id_email")).getAttribute("value");
        String oSupportEmail = driver.findElement(By.id("id_support_email")).getAttribute("value");
        String oBccEmail = driver.findElement(By.id("id_bcc_email")).getAttribute("value");
        String oTimezone = driver.findElement(By.id("id_timezone")).getAttribute("value");


    }
}
