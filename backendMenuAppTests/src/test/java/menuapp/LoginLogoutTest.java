package menuapp;

import com.MenuAppPO;
import com.UserPassMasterKey;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class LoginLogoutTest extends UserPassMasterKey {

    public WebDriver driver;
    MenuAppPO menuAppPO;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("default");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        menuAppPO = new MenuAppPO(driver);
        menuAppPO.getURL();
        wait = new WebDriverWait(driver, 8);
    }

    @Test
    public void loginAndLogoutPages() throws IOException {

        menuAppPO.loginMenuAppBackEnd_MPQA();

        menuAppPO.logoutFromMenuApp();

        wait.until(ExpectedConditions.urlToBe("http://qa-1-v5.menuapp.com/login/"));

        menuAppPO.selectWhatIsMenuPadLink();

        wait.until(ExpectedConditions.urlToBe("https://www.menupad.com/"));

        driver.navigate().back();

        wait.until(ExpectedConditions.urlToBe("http://qa-1-v5.menuapp.com/login/"));

        menuAppPO.selectForgotPassword();

        wait.until(ExpectedConditions.urlToBe("http://qa-1-v5.menuapp.com/users/password/reset/"));

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_email")));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
