package suitemenuapp;

import menuapp.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({

        CancelButtonTest.class,
        DashboardTest.class,
        LocationsTest.class,
        LoginLogoutTest.class,
        MenuItemsCreateTest.class,
        MenuItemsEditTest.class,
        OptionGroupsCreateTest.class,
        OptionGroupsEditTest.class,
        OptionGroupsItemSpecificTest.class,
        RegionsTest.class,
        RulesCreateTest.class,
        RulesEditTest.class,
        SectionCreateTest.class,
        SectionEditTest.class,
        SurveyCreateTest.class,
        SurveyEditTest.class

})
public class MenuAppSuite {
}
