package com;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import static com.thoughtworks.selenium.SeleneseTestBase.assertEquals;

public class MenuAppPO extends UserPassMasterKey {

    public String sectionName = "TestMenuSection";
    public String adminLocations = "all locations";
    public String admin = "Admin";
    public String page = "Page 1";
    public String item = "TestItem";
    public String rule = "TestRule";
    public String modName = "mod1";
    public String itemToEdit = "ItemEdit";
    public String optionValue = "Add a soup or salad";
    public String optionGroupName = "TestOptionGroup";
    public String survey = "TestSurvey";
    public String regionName = "TestRegion";
    public static final String PAGE_URL = "http://qa-1-v5.menuapp.com/login/";

    public final WebDriver driver;
    public WebDriverWait wait;

    public MenuAppPO(WebDriver aDriver) {
        driver = aDriver;
        wait = new WebDriverWait(driver, 8);
    }

    public void getURL() {
        driver.get(PAGE_URL);
    }


    public void loginMenuAppBackEnd_MPQA() throws IOException {

        driver.findElement(By.id("id_username")).sendKeys(USERNAME_MENUAPP);

        driver.findElement(By.id("id_password")).sendKeys(PASSWORD_MENUAPP);

        driver.findElement(By.cssSelector("button[type='submit']")).click();

        String welmsg = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("welcomeh1"))).getText();

        if(welmsg.contains("Welcome")) {
            // do nothing - user is signed in
        } else {
            throw new IOException("Invalid Login");
        }

        new WebDriverWait(driver, 5).until(ExpectedConditions.presenceOfElementLocated(By.id("username-icon")));
    }

    public void loginMenuAppBackEnd_Carmel() throws IOException {

        driver.findElement(By.id("id_username")).sendKeys(USERNAME_MENUAPP_CARMEL);

        driver.findElement(By.id("id_password")).sendKeys(PASSWORD_MENUAPP_CARMEL);

        driver.findElement(By.cssSelector("button[type='submit']")).click();

        String welmsg = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("welcomeh1"))).getText();

        if(welmsg.contains("Welcome")) {
            // do nothing - user is signed in
        } else {
            throw new IOException("Invalid Login");
        }

        new WebDriverWait(driver, 5).until(ExpectedConditions.presenceOfElementLocated(By.id("username-icon")));
    }
    public void selectDashboardFromNavBar() {
        driver.findElement(By.linkText("Dashboard")).click();
    }

    public void selectAnalyticsFromNavBar() { driver.findElement(By.linkText("Analytics")).click(); }

    public void selectNavLogo() {
        driver.findElement(By.className("navbar-brand")).click();
    }

    public void verifyMenuPageLoads() throws IOException {

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".sidebar-title>span")));

        String welcomeMessage = null;
        try {
            welcomeMessage = driver.findElement(By.cssSelector(".sidebar-title>span")).getText();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        if(welcomeMessage.contains("Menus")) {

        } else {
            throw new IOException("Menus page did not load!");
        }
    }

    public void selectMenuManagerLinkInNavBar() {
        driver.findElement(By.linkText("Menu Manager")).click();
    }

    public void selectMenus() {
        driver.findElement(By.linkText("Menus")).click();
    }

    public void verifyOptionGroupsPageLoads() throws IOException {
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("section-name"), "Add Option Group"));
    }

    public void verifyOptionGroupTemplatesPageLoads() throws IOException {

    }

    public void verifyRulesPageLoads() throws IOException {


    }

    public void verifyPOSImportPageLoads() throws IOException {

    }

    public void verifyInfoBannerIsSetToAllLocations() {

        assertEquals("You are editing MenuPad settings/data for all locations.", driver.findElement(By.cssSelector(".admin.info-banner")).getText());
    }

    public void selectSave() {
        // Select 'Save'
        driver.findElement(By.cssSelector("button[type='submit']")).click();
    }


    public void verifyYourChangesHaveBeenSavedMessage() {
        // Verify Saved message appears
        new WebDriverWait(driver, 5).until(ExpectedConditions.textToBePresentInElementLocated(By.className("confirm"), "Your changes have been saved."));
    }

    public void selectStoreDisplayDropdown() {
        // Select Store Display Dropdown
        driver.findElement(By.id("store_display")).click();
    }

    public void acceptAlert() {
        // Accept Alert
        driver.switchTo().alert().accept();
    }

    public void selectStoreLocation() {
        selectStoreDisplayDropdown();
        // Select Location
        driver.findElement(By.linkText(location)).click();
    }

    public void selectPage() {
        // Select Page
        driver.findElement(By.partialLinkText(page)).click();
    }

    public void selectSection() {
        // Select Section
        driver.findElement(By.partialLinkText(section)).click();
    }

    public void selectAddItem() {
        // Select "Add Item'
        driver.findElement(By.linkText("Add Item")).click();
    }

    public void selectAddPage() {
        // Select 'Add Page'
        driver.findElement(By.id("add-item-button")).click();
    }

    public void selectCancelButton() {
        // Tap 'Cancel'
        driver.findElement(By.id("item-cancel")).click();
    }

    public void selectCancel() {
        driver.findElement(By.linkText("Cancel")).click();
    }

    public void enterItemName() {
        // Enter Item Name
        driver.findElement(By.id("id_name")).sendKeys(item);
    }

    public void enterSectionName() {
        // Enter Section Name
        driver.findElement(By.id("id_name")).sendKeys(sectionName);
    }

    public void enterOptionGroupName() {
        // Enter Option Group Name
        driver.findElement(By.id("id_form-0-name")).sendKeys(optionGroupName);
    }

    public void selectSetupLocationsFromDashboard() {
        // Select 'Set up Locations'
        WebElement setUpLoc = driver.findElement(By.xpath("//tr/td[contains(text(), 'Set up Locations')]"));
        setUpLoc.click();
    }

    public void selectImportAndSetUpYourMenu() {
        // Select 'Import and set up your menu'
        WebElement importAndSetUpYourMenu = driver.findElement(By.xpath("//tr/td[contains(text(), 'Import and set up your menu')]"));
        importAndSetUpYourMenu.click();
    }

    public void selectUsersAndPermissionsFromDashboard() {
        // Select 'Users & Permissions'
        WebElement selectUsersAndPermissions = driver.findElement(By.xpath("//tr/td[contains(text(), 'Users')]"));
        selectUsersAndPermissions.click();
    }

    public void selectRulesFromDashboard() {
        // Select 'Rules'
        WebElement selectRules = driver.findElement(By.xpath("//tr/td[contains(text(), 'Rules')]"));
        selectRules.click();
    }

    public void selectSurveyFromDashboard() {
        // Select 'Survey'
        WebElement selectSurveys = driver.findElement(By.xpath("//tr/td[contains(text(), 'Surveys')]"));
        selectSurveys.click();
    }

    public void selectEditYourMenusFromDashboard() {
        // Select 'Edit your Menus'
        WebElement selectEditYourMenus = driver.findElement(By.xpath("//tr/td[contains(text(), 'Edit your Menus')]"));
        selectEditYourMenus.click();
    }

    public void selectImportManagerFromDashboard() {
        // Select 'Import Manager'
        WebElement selectImportManager = driver.findElement(By.xpath("//tr/td[contains(text(), 'Import Manager')]"));
        selectImportManager.click();
    }

    public void selectThemesFromDashboard() {
        // Select 'Themes'
        WebElement customizeMPBrandingButton = driver.findElement(By.xpath("//tr/td[contains(text(), 'Customize MenuPad Branding')]"));
        customizeMPBrandingButton.click();
    }

    public void logoutFromMenuApp() {
        selectStoreDisplayDropdown();
        // Select 'Logout'
        driver.findElement(By.linkText("Logout")).click();
    }

    public void selectWhatIsMenuPadLink() {
        // Select 'What is MenuPad?' link
        driver.findElement(By.linkText("What is MenuPad?")).click();
    }

    public void selectForgotPassword() {
        // Select 'Forgot Password'
        driver.findElement(By.linkText("Forgot Password?")).click();
    }

    public void selectDeleteButton() {
        // Select 'Delete'
        driver.findElement(By.linkText("Delete")).click();
    }

    public void selectDeleteItem() {
        // Select 'Delete Item'
        driver.findElement(By.cssSelector("div[name='delete']")).click();
    }

    public void tapSectionInBreadcrumb() {
        // Tap Section in breadcrumb
        driver.findElement(By.linkText(section)).click();
    }

    public void selectItemCategories() {
        // Select 'Item Categories'
        driver.findElement(By.cssSelector("div[data-form='item-categories']")).click();
    }

    public void selectFoodOptions() {
        // Select 'Food Options'
        driver.findElement(By.cssSelector("div[data-form='food-options']")).click();
    }

    public void selectItemMatching() {
        // Select 'Item Matching'
        driver.findElement(By.cssSelector("div[data-form='item-matching']")).click();
    }

    public void selectItemRules() {
        // Select 'Item Rules'
        driver.findElement(By.cssSelector("div[data-form='item-rules']")).click();
    }

    public void deleteOptionGroup() {
        // Select 'Delete Option Group'
        driver.findElement(By.linkText("Delete Option Group")).click();
    }

    public void selectAddOptionGroup() {
        // Select 'Add Option Group'
        driver.findElement(By.id("add-optiongroup")).click();
    }

    public void selectApplyOptionGroup() {
        // Select 'Apply Option Group'
        driver.findElement(By.id("option_group_apply")).click();
    }

    public void selectRules() {
        // Select 'Rules'
        driver.findElement(By.linkText("Rules")).click();
    }

    public void selectDeleteRule() {
        // Select 'Delete Rule'
        driver.findElement(By.linkText("Delete Rule")).click();
    }

    public void expandRulesAvailability() throws InterruptedException {
        // Expand 'Availability'
        driver.findElement(By.cssSelector("div[data-target='form div.row.rule-availability']")).click();
        Thread.sleep(1000);
    }

    public void expandRulesPriceAndVisibility() throws InterruptedException {
        // Expand 'Price and Visibility'
        driver.findElement(By.cssSelector("div[data-target='form div.row.rule-price']")).click();
        Thread.sleep(1000);
    }

    public void selectSundayRule() {
        // Select 'Sun'
        driver.findElement(By.id("id_sunday")).click();
    }

    public void selectEditPage() {
        // Select 'Edit Page'
        driver.findElement(By.partialLinkText("Edit Page")).click();
    }

    public void selectDeletePage() {
        // Delete Page
        driver.findElement(By.linkText("Delete Page")).click();
    }

    public void selectAdminLocation() {
        selectStoreDisplayDropdown();
        // Select Location - 'Admin'
        driver.findElement(By.linkText(admin)).click();
    }

    public void selectRulesDropdown() {
        // Select 'Rules' dropdown
        driver.findElement(By.cssSelector(".row.collapsed")).click();
    }

    public void selectOptionGroupsFromNavBar() {
        selectMenuManagerLinkInNavBar();
        // Select 'Option Groups'
        driver.findElement(By.linkText("Option Groups")).click();
    }

    public void selectLocationsFromNavBar() {
        selectCompanyLinkInNavBar();
        // Select 'Locations'
        driver.findElement(By.linkText("Locations")).click();
    }

    public void selectRegionsFromNavBar() {
        selectCompanyLinkInNavBar();
        // Select 'Regions'
        driver.findElement(By.linkText("Regions")).click();
    }

    public void selectCompanyLinkInNavBar() {
        driver.findElement(By.linkText("Company")).click();
    }

    public void verifyLocationsPageLoads() {
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("sidebar-title"), "Locations"));
    }

    public void deleteSurvey() {
        // Select 'Delete Survey'
        driver.findElement(By.linkText("Delete Survey")).click();
    }

    public void selectAddButton() {
        // Select 'Add'
        driver.findElement(By.linkText("Add")).click();
    }

    public void addRegionName() {
        Random rand = new Random();
        int value = rand.nextInt(500);

        // Add Region Name
        driver.findElement(By.id("id_name")).sendKeys(regionName+value);
    }

    public void deleteRegion() throws InterruptedException {
        List<WebElement> regionAfter = driver.findElements(By.partialLinkText("Region"));
        for (int i=0; i<regionAfter.size(); i++) {
            driver.findElement(By.partialLinkText(regionName)).click();
            driver.findElement(By.partialLinkText("Delete Region")).click();
            acceptAlert();
            verifyYourChangesHaveBeenSavedMessage();
            Thread.sleep(2000);
        }

    }

    public void turnOnPrintChitsInMenuapp() {

        driver.findElement(By.linkText("Company")).click();

        // Select on "Locations"
        driver.findElement(By.linkText("Locations")).click();

        // Select Location
        driver.findElement(By.linkText(location_CARMEL)).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#save_form")));

        // Select 'iPad Settings'
        driver.findElement(By.xpath(".//*[@id='content-wrapper']/div/div[2]/div[2]/div[2]/span")).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id("id_promo_pages")));

        WebElement mySelectElm = driver.findElement(By.id("id_order_functionality"));
        Select mySelect= new Select(mySelectElm);

        // Print Option selected
        WebElement optionSelected = mySelect.getFirstSelectedOption();
        String optSel = optionSelected.getText();
        System.out.println("Print Chit Option selected: " + optSel);

        if (!optionSelected.getAttribute("value").contains("print_only")) {
            mySelect.selectByValue("print_only");
        }
    }

    public void turnOffPrintChitSettings() throws IOException {

        driver.findElement(By.linkText("Company")).click();

        // Select on "Locations"
        driver.findElement(By.linkText("Locations")).click();

        // Select Location
        driver.findElement(By.linkText(location_CARMEL)).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#save_form")));

        // Select 'iPad Settings'
        driver.findElement(By.xpath(".//*[@id='content-wrapper']/div/div[2]/div[2]/div[2]/span")).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id("id_promo_pages")));

        WebElement mySelectElm = driver.findElement(By.id("id_order_functionality"));
        Select mySelect= new Select(mySelectElm);

        // Print Option selected
        WebElement optionSelected = mySelect.getFirstSelectedOption();
        String optSel = optionSelected.getText();
        System.out.println("Print Chit Option selected: " + optSel);

        if (!optionSelected.getAttribute("value").contains("place_orders")) {
            mySelect.selectByValue("place_orders");
        }
    }

    public void selectiPadSettings() {
        // Select 'iPad Settings'
        driver.findElement(By.xpath(".//*[@id='content-wrapper']/div/div[2]/div[2]/div[2]/span")).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.id("id_promo_pages")));
    }
}
